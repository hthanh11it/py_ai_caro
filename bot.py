
import random

class Bot:
    def __init__(self, player=1, moved_count=0):
        self.player = player
        self.moved_count = moved_count

    def __str__(self):
        return "Player: %s \n Moved count: %s" % (self.player, self.moved_count)

    def __repr__(self):
        return {'player' : self.player, 'moved_count' : self.moved_count}

    def update_moved_count(self, moved_count):
        self.moved_count = moved_count
    
    def move(self, board):
        raise NotImplementedError("Implement this to build your own AI.")
        pass

    @property
    def bot_kind(self):
        return self.__class__.__name__
