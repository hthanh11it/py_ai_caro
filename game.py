import sys
from config import *
from rules import Referee
from player import Player
from mybot import MySmartBot
import tkinter as tk
from common import Point
from  tkinter import *


def GUI(BOARD, PLAYER, NTH_MOVE, PLAYER_MOVED_COUNT):
    X_TO_CHAR = {i: chr(ord('A')+i) for i  in range(20)}

    window = Tk()

    window.title("Game connect6")

    # Doc
    Label(window, text = "", height = 2, width = 3, highlightbackground="white", fg="black").grid(row = 0, column = 0)
    for i in range(19):
        Label(window, text = str( X_TO_CHAR[i] ), height = 2, width = 3, highlightbackground="white", fg="black").grid(row = 0, column = 1+i)

    # Ngang
    for t in range(19):
        Label(window, text = str(t+1), height = 2, width = 3, highlightbackground="white", fg="black").grid(row = 1+t,column = 0)
    


    # Click GUI
    def action(button, moveGui):
        toa_do = moveGui

        global  BOARD, PLAYER, NTH_MOVE, PLAYER_MOVED_COUNT

        NTH_MOVE += 1
        
        
        bot_set[PLAYER].update_moved_count(PLAYER_MOVED_COUNT) 

        x, y = Point.from_name(toa_do)
        able_to_place, msg = referee.can_place(x, y) #  point có thể đi

        # gán play 1 2 với toạ độ bàn cờ
        BOARD[y][x] = PLAYER

        referee.update(x, y, PLAYER) 

        # CMD bàn cờ
        draw_board(BOARD, PLAYER, NTH_MOVE) 

        won_player = referee.determine()
        if won_player is not None:
            
            # show popup win exit
            exit_game(bot_set[won_player])
        

        if (PLAYER_MOVED_COUNT < 2 & PLAYER == 2):
            btns[button].configure(text='X')
        else:
            btns[button].configure(text='O')

        PLAYER_MOVED_COUNT += 1 # player đi 2 lần 
        if PLAYER_MOVED_COUNT == 2:
            PLAYER_MOVED_COUNT = 0
            PLAYER = 2 if PLAYER == 1 else 1

        
        return moveGui

    # Click GUI
    

    btn_nr = -1
    btns = []

    for doc in range(1, 20):

        for ngang in range(1, 20):

            btn_nr += 1

            btns.append(Button(text =' ', height = 2, width = 3, highlightbackground="white", fg="black", 
                command=lambda x=btn_nr, moveGui="{}{}".format( X_TO_CHAR[doc-1], ngang): action(x,moveGui)))

            btns[btn_nr].grid(row=ngang, column=doc)

    exit_button = Button(text='Exit Game', command=window.destroy)
    exit_button.grid(row=1, column=22, columnspan=15)

    window.mainloop()


if __name__ == '__main__':
    # Set default
    whitebot = Player(1)
    blackbot = Player(2)

    

    bots = [whitebot, blackbot]
    
    global BOARD, PLAYER, NTH_MOVE, PLAYER_MOVED_COUNT
    bot_set = [None] + bots
    referee = Referee(BOARD)
    
    GUI(BOARD, PLAYER, NTH_MOVE, PLAYER_MOVED_COUNT)
 