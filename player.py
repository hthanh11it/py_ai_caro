from config import *
from bot import Bot
from common import Point

class Player(Bot) :
    def move(self, board) :

        # Get value (1 or 2) from GUI
        move = input('{} turn : '.format(STONE_NAME[self.player]))
        # Get value (1 or 2) from GUI
        
        '''
        # Get value (1 or 2) from CMD
        move = input('{} turn : '.format(STONE_NAME[self.player]))
        # Get value (1 or 2) from CMD
        '''
        return Point.from_name(move)
