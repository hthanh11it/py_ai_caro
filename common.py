from config import *
from collections import namedtuple

class Point(namedtuple('Point', ['x', 'y'])) :
    @staticmethod
    def from_x_y(x,y):
        return Point(x, y)
        
    @staticmethod
    def from_name(name):
        x = CHAR_TO_X[name.capitalize()[0]]
        y = int(name[1:])-1
        return Point(x, y)

    @property
    def name(self):
        return X_TO_CHAR[self.x] + str(self.y+1)

    __slots__ = ()

    def __str__(self):
        return '{} ({},{})'.format(self.name, self.x, self.y)
