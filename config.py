import sys
import os
from  tkinter import messagebox

NUM_BOARD = 19
WIN_NUMBER = 6
MAX_ITERS = 100000
MAX_TIMEOUT = 1
PLAYER = 1  # 1=white 2=black. white moves first
PLAYER_MOVED_COUNT = 1
NTH_MOVE = 1
BOARD = [[0 for x in range(NUM_BOARD)] for y in range(NUM_BOARD)]
STONE_CHAR = ['.', 'O', 'X']
STONE_NAME = ['', 'White (O)', 'Black (X)']
CHAR_TO_X = {chr(ord('A') + i) : i for i in range(NUM_BOARD)}
X_TO_CHAR = {i: chr(ord('A')+i) for i in range(NUM_BOARD)}
DIRECTIONS = [
    lambda x, y: (x-1, y-1),
    lambda x, y: (x, y-1),
    lambda x, y: (x+1, y-1),
    lambda x, y: (x+1, y),
]
DIRECTIONS_OPPOSITE = [
    lambda x, y: (x+1, y+1),
    lambda x, y: (x, y+1),
    lambda x, y: (x-1, y+1),
    lambda x, y: (x-1, y),
]



def is_outta_range(x, y):
    return x < 0 or x >= NUM_BOARD or y < 0 or y >= NUM_BOARD

# doi thanh popup
def exit_game(won_bot=None):
    if won_bot is not None:
        if won_bot == 0 :
            print('there is no split win or lose!')
        else :
            print('{} ({}) won!!'.format(STONE_NAME[won_bot.player], won_bot.bot_kind))

            messagebox.showinfo (title="Player win!", message=f'{STONE_NAME[won_bot.player]} ({won_bot.bot_kind}) won!!')
    else:
        print('No one won.')
    #sys.exit()


def darktext(str):
    return str if os.name == 'nt' else '\x1b[0;30m{}\x1b[0m'.format(str)

def draw_board(board, player=0, nth_move=0):
    os.system('cls' if os.name == 'nt' else 'clear')
    
    print('Move : {}'.format(nth_move))
    print('{} turn.'.format(STONE_NAME[player]))
    print()
    print('       {} '.format(' '.join(str(e) for e in CHAR_TO_X)))
    print('     +-{}-+'.format('-'.join('-' for e in CHAR_TO_X)))
    
    for y in range(NUM_BOARD):
        print('  {:>2d} |'.format(y+1), end='')  # line no.
        for x in range(NUM_BOARD):
            stone = board[y][x]
            if stone != 0:
                print(' ' + STONE_CHAR[board[y][x]], end='')
            else:
                print(darktext('  '), end='')
        print(' |')

    print('     +-{}-+'.format('-'.join('-' for e in CHAR_TO_X)))
    print()