import sys
from config import *



def reverse_of(dir_func):
    dx, dy = dir_func(0, 0)
    return lambda x, y: (x-dx, y-dy)

class Referee:
    def __init__(self, initial_board):
        self.board = initial_board
        self.last_move = None
        self.list_move = []
        
    def __str__(self):
        return "board: %s\n last_move: %s" % (self.board, self.last_move)

    def __repr__(self):
        return {'board' : self.board, 'last_move' : self.last_move}
    
    def update(self, x, y, player):
        self.board[y][x] = player
        self.last_move = [x, y]
        self.list_move.append( [x, y] )

    def determine(self) -> int :
        board = self.board
        x, y = self.last_move
        for i in range(4) :
            step = 1

            nx, ny = [x, y]
            while True :
                ny, nx = DIRECTIONS[i](ny, nx)
                if is_outta_range(ny, nx) : break
                if board[ny][nx] != board[y][x] : break
                step += 1
            if step >= WIN_NUMBER : return board[y][x]

            nx, ny = [x, y]
            while True :
                ny, nx = DIRECTIONS_OPPOSITE[i](ny, nx)
                if is_outta_range(ny, nx) : break
                if board[ny][nx] != board[y][x] : break
                step += 1
            if step >= WIN_NUMBER : return board[y][x]

        return None

    def can_place(self, x, y):
        if is_outta_range(x, y):
            return False, 'Wrong input'

        if self.board[y][x] != 0:
            return False, 'Duplicated move'

        return True, 'Ok'

    def get_can_place(self, x, y):
        if is_outta_range(x, y):
            return False

        if self.board[y][x] != 0:
            return False

        return True

    # def board_can_place(self):
        # board_place = []
        # for x in range(NUM_BOARD) :
        #     for y in range(NUM_BOARD) :
        #         if self.board[x][y] == 0 :
        #             board_place.append( [x, y] )
        # return board_place
