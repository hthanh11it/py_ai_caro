# from dumper import *
from config import *
from bot import Bot
from rules import Referee
from common import Point
import time
import random
import copy



def backpropagation_point( root_node, list_current_node, child, won, point=0) :
    if won == 0 :
        point += 0.5
    if won == child.player :
        point += 1

    if child.parent == root_node :
        for current in list_current_node :
            if current == child :
                current.point += point
                return
    
    if child.parent is not None :
        for current in list_current_node :
            if current == child.parent :
                backpropagation_point( root_node, list_current_node, current, won, point)
                return
    

def check_place_in_parent( place, parent, root_node ) :
    if parent == root_node : return False
    if parent.move == place : return True
    return check_place_in_parent( place, parent.parent, root_node )

def simulator_win_lose( player, moved_count, list_board_can_place, board ) :
    if len(list_board_can_place) <= 0 : return 0

    referee = Referee(board)
    place = random.choice( list_board_can_place )
    list_board_can_place.remove( place )

    px,py = place
    referee.update( px, py, player )
    won_player = referee.determine()
    if won_player is not None:
        return won_player

    board[py][px] = player
    moved_count += 1
    if moved_count == 2:
        moved_count = 0
        player = 2 if player == 1 else 1

    del referee, place, px, py, won_player
    return simulator_win_lose( player, moved_count, copy.deepcopy(list_board_can_place), copy.deepcopy(board) )

class Node:
    def __init__(self, board=None):
        self.parent = None
        self.child = []
        self.player = None
        self.moved_count = None
        self.point = 0
        self.board = board
        self.move = [None,None]

    def set_move(self, place=[0,0]) :
        x,y = place
        if x != None and y != None :
            self.move = [x, y]
            self.board[y][x] = self.player

    def clone(self) :
        node = Node()

        player = self.player
        moved_count = self.moved_count + 1
        if moved_count >= 2 :
            player = 2 if player == 1 else 1
            moved_count = 0

        node.player = player
        node.moved_count = moved_count
        node.board = copy.deepcopy(self.board)
        return node

    def get_place_parent(self, list_current_node, list_board_can_place, root_node) :
        place_select = None
        parent_select = None
        if len(list_board_can_place) > 0 :
            for _ in range(100) :
                list_board_can_place_clone = copy.deepcopy(list_board_can_place)
                place = random.choice(list_board_can_place_clone)
                parent = random.choice(list_current_node)
                if not check_place_in_parent( place, parent, root_node ) :
                    place_select = place
                    parent_select = parent
                    break

        if place_select is None or parent_select is None :
            for _ in range(100) :
                select = random.choice(list_current_node)
                place = select.move
                select = random.choice(list_current_node)
                parent = select
                if not check_place_in_parent( place, parent, root_node ) :
                    place_select = place
                    parent_select = parent
                    break

        if place_select is None or parent_select is None :
            select = random.choice(list_current_node)
            place_select = select.move
            parent_select = select.parent
            
        return place_select, parent_select

def MCTS( player, moved_count, board ) :
    root_node = Node( board )
    root_node.player = player
    root_node.moved_count = moved_count

    list_current_node = [root_node]
    list_board_can_place = []
    for x in range(NUM_BOARD) :
        for y in range(NUM_BOARD) :
            if root_node.board[y][x] == 0 :
                list_board_can_place.append([x,y])

    visit_iter = 0
    duration = 0
    while visit_iter <= MAX_ITERS and duration <= MAX_TIMEOUT :
        start = time.process_time()

        # 1. Selection
        child = root_node.clone()
        place, child.parent = child.get_place_parent( list_current_node, list_board_can_place, root_node )
        child.set_move( place )

        # 2. Expansion
        if child not in list_current_node :
            list_current_node.append( child )
        if place in list_board_can_place :
            list_board_can_place.remove(place)

        # 3. Simulation
        player = child.player
        moved_count = child.moved_count
        moved_count += 1
        if moved_count == 2:
            moved_count = 0
            player = 2 if player == 1 else 1
        won = simulator_win_lose( player, moved_count, copy.deepcopy(list_board_can_place), copy.deepcopy(child.board) )

        # 4. Backpropagation
        backpropagation_point( root_node, list_current_node, child, won, 0)

        visit_iter += 1
        duration += time.process_time() - start

    point = 0
    move = []
    for node in list_current_node :
        if node.parent == root_node :
            if node.point > point :
                point = node.point
                move = node.move

    del root_node, list_current_node, list_board_can_place, visit_iter, duration, start, child, place, player, moved_count, won, point
    return move

class MySmartBot(Bot) :
    def move(self, board) :
        x, y = MCTS( self.player, self.moved_count, copy.deepcopy(board) )

        return Point.from_x_y( x, y )